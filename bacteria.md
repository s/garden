# Bacteria

- Bacterial flagellum
  - Most commonly, one flagellum creates a "run and twiddle" movement where
  bacteria can achieve [[chemotaxis]]
  - [[Spirochaeta]] have their flagellum throughout the inside of the cell,
  which allows movement through viscous materials
- By default, bacteria are Gram-negative, except for [[Fermicutes]]:
Gram-negative bacteria form a [[symplesiomorphy]]

## Taxonomy

- [[Bacteria]]
  - [[Terrabacteria]]
    - [[Cyanobacteria]]
    - [[Fermicutes]] (Gram-positive bacteria)
  - [[Hydrobacteria]]
    - [[Spirochaeta]]
    - Proteo-Plancto MRCA
      - [[Proteobacteria]]
      - [[Planctobacteria]]
        - [[Chlamydiae]]
