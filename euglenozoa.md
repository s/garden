# Euglenozoa

- Members of the [[Excavata]]
- Euglena Gracilis
  - Photosynthetic, free-living
- Typanosoma Cruzi (Chagas Disease) and Trypanosoma Brucei (African Sleeping
Sickness)
  - Nonphotosynthetic, parasitic
  - Transmitted by arthropod vectors
  - Because they're eukaryotic and aerobic, they're hard to target with
  medication
