# Tetrapodomorpha

- Member of the [[Sarcopterygii]] in the [[Osteichthyes]]
- The [[Vertebrata]] which made a transition onto dry land
  - Due to the cooler and dryer Earth during the [[Carboniferous]], it was hard
    to stay in freshwater lakes
  - Developed the [[Amniote egg]]

## Taxonomy

- [[Tetrapodomorpha]]
  - Modern Amphibians
  - [[Amniota]]
