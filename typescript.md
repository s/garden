# TypeScript

- A [[programming-language|programming language]].
- Aims to add [[static typing]] to [[JavaScript]].
