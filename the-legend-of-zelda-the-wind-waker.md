# The Legend of Zelda: The Wind Waker

- A [[video-game|video game]] released on December 13, 2002 for the Nintendo GameCube.
- Part of [[the-legend-of-zelda|The Legend of Zelda]] series.
