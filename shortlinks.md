# Shortlinks

- A way of referencing URLs without typing out the full domain.
- Common shorthands for websites are mutually agreed upon.
- Commonly:
  - `gh/[user]/[repo]#[issue]` links to
    `https://github.com/[user]/[repo]/issues/[issue]`: `repo` and `issue` are
    omittable in the case that only a user or only a repository are wanted to
    be linked to.
  - Same for `gl` but to GitLab instead of GitHub.
  - Same for `gd` but to GitDab.
  - `tw/[user]` links to `https://twitter.com/[user]`.
  - `sw/[id]` links to
    `https://steamcommunity.com/sharedfiles/filedetails/?id=[id]`.
- References to the [[Agora]] could also be made shortlinks, perhaps
  `ag/[node]`?
- Shortlinks aim to emulate the automatic link replacements of the website
  in question.
  - `gh/[content]` should behave the same as if `[content]` was posted on
    GitHub itself.
  - `tw/[user]` should replace the functionality of mentioning `@user` on
    Twitter.

## Shortlink Guidelines

- Shortlinks should start with an identifier and a slash, such as `tw/` or 
  `abc/`.
  - The identifier is not restricted to two characters, but most common ones
    can be condensed to two characters.
- Shortlinks should not be URIs themselves.
- Shortlinks should not obfuscate content, like a traditional link shortener.
