# Endothermy

- Present in [[Mammalia]] and [[Aves]]
- [[Brown fat]] [[mitochondrion]]
  - Puts food and oxygen towards generating heat instead of ATP
- Maintaining body temperature is important for [[enzymes]] to operate
- Metabolic rate _decreases_ when temperature increases, the direct opposite
  of [[ectothermy]]
- [[Fish]] swimming styles
  - [[Tuna]]: [[Thunniform swimming]] with a narrow [[caudal peduncle]] and a
    sickle-shaped tail, leaves less turbulence behind and uses less energy
  - [[Trout]]: [[Subcaraigiform swimming]] leaves turbulence behind but allows
    more mobility
  - Most kinds of fish are [[ectothermy|ectothermic]]
  - Thunniform swimmers have the muscles they use on their inside, so heat
    is generated internally, where most fish have their swimming muscles on
    the outside which get cooled by the water
  - This elevated body temperature allows them to be fast thinkers and movers
- Endothermy is costly
  - We expect endotherms to reduce the costs via adaptations
  - We expect endothermy to only evolve when the costs can be offset
  - Minimizing heat loss
    - [[Gigantothermy]]: The bigger you are, the less surface area there is to
      lose body temperature to
    - Allow temperature to drop purposefully during some times in some places
      - [[Hibernation]]
      - Reducing circulation
    - [[Insulation]]: Hot-body radiation only occurs at the surface, so if
      you can prevent heat from reaching the surface, you can prevent
      hot-body radiation
      - [[Feathers]]: When you pick up a bird it will be cold, although without
        its feathers it would be warm
    - [[Countercurrent Heat Exchange]]: If the blood on the way back from
      an extremity would be cold, wrap the veins around the arteries so the
      returning blood warms up while the leaving blood cools
