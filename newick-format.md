# Newick Format

- Format to represent trees with distances and branch names

# Examples

Printed using DendroPY options
`show_internal_node_labels = True`, `width=40`, `plot_metric="length"`.

## Single Descendant

```
(B)A;
```
```
A------------------------------------- B
```

## Two Descendants

```
(B,C)A;
```
```
/------------------------------------- B
A
\------------------------------------- C
```

## Two Levels Of Descendants

```
((D,E)B,(F,G)C)A;
```
```
                   /------------------ D
/------------------B
|                  \------------------ E
A
|                  /------------------ F
\------------------C
                   \------------------ G
```

## Distances

```
(A:1,B:2);
```
```
/------------------ A
@
\------------------------------------- B
```

## Big Example

```
(
    (
        (
            (
                Dasyatis_laosensis:0.00864712,
                Hemitrygon_akajei:0.02501331
            ):0.00555551,
            Dasyatis_sp._TT:0.01190329
        ):0.07844344,
        Maculabatis_gerrardi:0.10443933
    ):0.03362855,
    (
        Aetobatus_flagellum:0.06712780,
        (
            Aetobatus_narinari:0.00190402,
            Aetobatus_ocellatus:0.00159879
        ):0.05608476
    ):0.06364667,
    Squatina_squatina:0.24404039
);
```

```
         /Dasyatis laosensis
        /@
  /-----@\- Hemitrygon akajei
  |     |
/-@     \ Dasyatis sp. TT
| |
| \------- Maculabatis gerrardi
|
|   /----- Aetobatus flagellum
@---@
|   |    /Aetobatus narinari
|   \----@
|        \Aetobatus ocellatus
|
\------------------ Squatina squatina
```
