# Oomycota

- Members of the [[Heterokonta]] [[phylum]] of the [[sar-supergroup|SAR Supergroup]]
- Heterotrophic and fungus-like
- Fish parasites
- Phytophthora Infestans
  - Cause of Irish Potato Famine
