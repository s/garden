# Parabasalia

- Members of the [[Metamonada]] [[phylum]] of the [[Excavata]]
- Flagellated anaerobes, living within anaerobic spaces within other animals
- Mixotricha Paradoxa
  - Found in the gut of termites
  - Cellulose is not digestable by most animals
  - Mixotricha Paradoxa can break down cellulose before the termite digests it
    - Mixotricha Paradoxa doesn't produce cellulase
    - Unclear whether the termite actually digests the Mixotricha Paradoxa or
    lets it live
  - Uses own flagellum as rudders, while [[spirochaeta|spirochetes]] power the movement
- Trichomonas Vaginalis
  - Agent of STD in humans
