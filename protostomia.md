# Protostomia

- The [[blastopore]] develops into the mouth

## Taxonomy

- [[Protostomia]]
  - [[Lophotrochozoa]]
    - [[Annelida]]
    - [[Mollusca]]
    - Flatworms
  - [[Ecdysozoa]]
    - [[Arthropoda]]
    - [[Nematoda]]
