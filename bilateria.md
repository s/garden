# Bilateria

- A 3rd germ layer on top of [[Eumetazoa]], the [[mesoderm]]
- Bilaterally symmetric
- The [[Urbilaterian]]:
  - MRCA of the Bilaterians
  - Must have been extremely unspecialized to give rise to both
    [[deuterostomia|deuterostomes]] and [[protostomia|protostomes]]
- Anterior to Posterior positioning controlled by [[hox-genes|Hox Genes]]
  - In this case, they're called Hox cluster genes
