# Archaea

- Different from [[bacteria]] in the generation of the cell membrane
- All prokaryotic
- More closely related to [[eukarya]] than [[bacteria]]
- Newer findings imply that [[Lokiarchaeota]] are closer than the
[[tack-supergroup|TACK Supergroup]] to [[eukarya]]
- Vast majority are non-extremophile

## Taxonomy

- [[Archaea]]
  - [[Euryarchaeota]]
  - [[Proteoarchaeota]]
    - [[tack-supergroup|TACK Supergroup]]
      - [[Korarchaeota]]
      - TC MRCA
        - [[Thaumarchaeota]]
        - [[Crenarchaeota]]
    - [[Lokiarchaeota]]
      - Loki1/Loki2
      - Loki3/[[Eukarya]]
