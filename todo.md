# Todo

- [x] Add this garden to the [Agora](https://flancia.org/go/agora)
- [ ] Write a [[Ghidra]] plugin to output procedures as individual
  [[MASM Assembly]] files
- [x] Finish my contribution of `En_Tg` to the [[the-legend-of-zelda-ocarina-of-time|Ocarina of Time]]
  [[decompilation]] project
- [ ] Finish my addition of a `HashMap` to `cats-collections`.
- [ ] Document how I accomplished the Lecture Transcriber written on
  [[2020-11-02]].
