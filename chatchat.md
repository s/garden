# ChatChat

- "A game about being a cat."
- One of my favorite games growing up, my first introduction to unrestricted
  online interaction.
- I've messed around with this game quite a bit to discover its internals,
  [which I've documented on TCRF](https://tcrf.net/ChatChat).
- I'm working on a [[TypeScript]] reimplementation of the game so that it will
  run without Flash, [open-source on GitDab](https://gitdab.com/s/chatchat).
