# 2021-01-03

- Conceptualized an [[OpenGL]] loader in [[Zig]].
- Looked into solving non-euclidean Rubik's Cubes via [[MagicTile]].
