# Molecular Evolutionary Genetics Analysis

- [[software]]
- https://www.megasoftware.net/
- Analyzes gene sequences to reconstruct phylogenetic trees
  - Can create pair-wise distance matrices between organisms
    - Can be output as images
    - [[CSV]]
  - Can create cladograms
    - Can be output as images
    - [[newick-format|Newick Format]]
    - [[CSV]]
- Does alignment via [[nucleotides]] 
