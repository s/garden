# Spirochaeta

- Free-living and parasitic
- Includes agents of [[syphilis]] and [[Lyme disease]]
- Periplasmic flagella, where the flagellum runs through the inside of the
cell, allowing for movement through viscous materials
