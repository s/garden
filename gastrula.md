# Gastrula

- The result of [[cleavage-divisions|Cleavage Divisions]]
- Has an [[ectoderm]] and an [[endoderm]]
- Has a [[blastopore]] leading to an [[archenteron]]
