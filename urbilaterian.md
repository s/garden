# Urbilaterian

- Gave rise to the [[protostomia|protostomes]] and
  [[deuterostomia|deuterostomes]]
  - Had to be simple based on this
- Based on Hox genes common to most descendants:
  - The Urbilaterian most likely had 6 segments
  - It most likely had 2 eyes
  - It most likely had a heart
  - Had to be complex based on this
- Due to lack of fossil evidence, we don't know much for certain about it
