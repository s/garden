# Chordata

- Member of the [[Deuterostomia]]
- Most basal branch: [[Cephalochordata]]
  - Barely has a central nervous system
  - 5 synapomorphies
    - notochord
    - dorsal hollow nerve cord
    - endostyle
    - pharyngeal slits
    - post-anal tail
  - [[Invertebrate]]
- Main branch
  - [[Urochordata]]
  - [[Vertebrata]]
- First Chordates appeared in the [[Cambrian]]

## Taxonomy

- [[Chordata]]
  - [[Cephalochordata]]
  - [[Olfactores]]
    - [[Urochordata]]
    - [[Vertebrata]]
      - Jawless Fishes
        - Hagfishes
        - Lampreys
      - [[Gnathostomata]]
