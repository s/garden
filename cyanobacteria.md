# Cyanobacteria

- Responsible for [[the-great-oxygenation|The Great Oxygenation]]
- [[Spirulina]]: Highly nutritious
- [[Rivularia]]: Classifies as multi-cellular, as it can regrow its
photosynthetic filament
