# Choanoflagellatea

- A member of the [[Opisthokonta]]
- "Choano" refers to a collar surrounding the flagella
- Not considered an [[animalia|animal]] as there are no specialization
  mechanisms (technically unicellular, even if colonial)
- Food piles up at the bottom of the collar due to the movement of the
  choanoflagellate in the water
