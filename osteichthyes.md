# Osteichthyes

- Member of the [[Gnathostomata]] in the [[Vertebrata]]
- True bone
- Had swim bladders
  - [[fresh-water|Fresh water]] has a lower buoyant density than
    [[sea-water|sea water]]
  - Needed some way to offset the weight of the bone structures

## Taxonomy

- [[Osteichthyes]]
  - [[Actinopterygii]]
  - [[Sarcopterygii]]
    - [[Tetrapodomorpha]]
