# Euryarchaeota

- [[Methanogens]]: Pick up waste molecules in anaerobic locations, such as H₂,
CH₃CO₂H, HCO₂H, CH₄0
- Extreme Halophiles: Prefer extremely salty habitats
