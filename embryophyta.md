# Embryophyta

- Aka land plants
- [[alternation-of-generations|Alternation of Generations]]

## Taxonomy

- [[Embryophyta]]
  - Liverworts, Mosses, Hornworts
  - [[Polysporangiophyta]]
    - [[Eutracheophyta]]
