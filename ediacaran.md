# Ediacaran

- The period before the [[Cambrian]]
- The MRCAs of the [[Deuterostomia]], [[Lophotrochozoa]], and
  [[Ecdysozoa]] most likely split here
