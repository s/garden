# Eutracheophyta

- True vascular plants

## Taxonomy

- [[Eutracheophyta]]
  - [[Lycophytes]]
    - Club Mosses, Spike Mosses, Quillworts
  - [[Euphyllophytes]]
    - [[Monilophytes]]
      - Ferns, Horsetails, Whisk Ferns
    - [[Spermatophytes]]
      - [[Gymnosperms]]
      - [[Angiosperms]]
