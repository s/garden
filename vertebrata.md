# Vertebrata

- Member of the [[Chordata]]
- Replaces the notochord with a spine
- "New head"
  - Neural crest cells
- Transition to dry land started during the [[Devonian]]

## Taxonomy

- [[Vertebrata]]
  - Jawless Fishes
    - Hagfishes
    - Lampreys
  - [[Gnathostomata]]
