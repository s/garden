# Diatoms

- Members of the [[Heterokonta]] [[phylum]] of the [[sar-supergroup|SAR Supergroup]]
- Photosynthetic algae, with silica-rich shells
  - When diatomaceous earth is exposed, silica can be harvested from the
  remains of diatoms
