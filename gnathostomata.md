# Gnathostomata

- Member of the [[Vertebrata]] in the [[Chordata]]
- Allowed animals to much more easily be predators
  - Jaws could be used to immobilize prey
  - Once you become predatory, the fitness space becomes much more steep
    - Your fitness highly depends on your sight and agility
- First Gnathostomes appeared in the [[Silurian]]

## Taxonomy

- [[Gnathostomata]]
  - [[Placodermi]]
  - [[Eugnathostomata]]
    - [[Chondrichthyes]]
    - [[Osteichthyes]]
