# chalkbot

- A [[Discord]] bot written in [[Java]].
- chalkbot aims to support as many languages and locales as possible.
- chalkbot aims to "fill" the features that Discord is missing:
  - Storing timezones and retrieving times of users
  - Setting reminders for later dates
  - Managing pronoun roles
  - Having [[shortlinks]]
- chalkbot aims to use [[natural-language-processing|Natural Language Processing]] for commands to be
  ergonomic and multilingual.
