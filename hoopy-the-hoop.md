# Hoopy the Hoop

- Hoopy the Hoop is an inanimate character in [[Portal]] and [[Portal 2]].
- The developers of Portal and Portal 2 wanted to make Hoopy the Hoop become
  a sort of meme.
- Hoopy the Hoop is not widely known about, while "The Cake is a Lie" is now
  one of the most pervasive memes in internet history.
