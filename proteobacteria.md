# Proteobacteria

- Incredibly diverse in shape, physiology, and ecology

## Taxonomy

- [[Proteobacteria]]
  - [[Acidobacteria]]
  - αβγδεζ
    - Deltaproteobacteria
    - αβγεζ
      - Epsilonproteobacteria
      - αβγζ
        - Mitochondrial Ancestor
          - [[Alphaproteobacteria]]
          - [[Mitochondrion]]
        - βγζ
          - Zetaproteobacteria
          - βγ
            - Betaproteobacteria
            - Gammaproteobacteria
