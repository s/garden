# Protista

- Not a valid taxon, as it is a [[paraphyly]]
- Refers to all [[Eukarya]] that are _not_ [[Animalia]], [[Embryophyta]], or
[[Fungi]]
