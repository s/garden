# Geologic Timeline

- [[Hadean]]: 4.571 BYA to 4.0 BYA
  - Named due to hellish conditions
- [[Archaean]]: 4.0 BYA to 2.5 BYA
  - Reasonably condusive to life
  - Pretty certain that cellular life originated here
  - 3.48 BYA: First cellular life, [[Stromatolites]]
  - 3 BYA: Oxygenic photosynthesis defines the end of the Archaean
- [[Proterozoic]]: 2.5 BYA to 541 MYA
  - 2.8 BYA to 1.9 BYA: Banded Iron Formations imply periods of high
  oxygenation
  - CH₄ (Methane) + 2O₂ → CO₂ + 2H₂O: Methane concentration decreased, while
  Oxygen and CO₂ increased
  - [[huronian-glaciation|Huronian Glaciation]]
  - [[Ediacaran]]
- [[Phanerozoic]]: 541 MYA to Present
  - Started with a mass-extinction event
  - Divided into [[Paleozoic]], [[Mesozoic]], and [[Cenozoic]]
