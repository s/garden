# The Great Oxygenation

- The diverity of [[bacteria]] exploded during the [[Archaean]], leading to the
evolution of [[Cyanobacteria]]
  - This does not mean that [[Spirulina]] existed during the [[Archaean]]: it
  is a modern organism
- There was almost no O₂ in the atmosphere before the
[[Archaean]]-[[Proterozoic]] break
  - No O₂ means no formation of peroxides
  - Peroxides are dangerous to many cells as they rob electrons from
  proteins, fatally altering them
- Cells had to develop anti-oxidation defenses, including [[catalase]]
- Cells had two ways to adapt to O₂
  1. Become a strict anaerobe, and avoid oxygen-containing habitats
  2. Tolerate O₂
     - O₂ toleration can easily lead to aerobic metabolism
