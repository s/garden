# Fungi

- It used to be presumed that [[Fungi]] were more [[Embryophyta]]-like than
[[Animalia]]-like, because of their plant-like structure and cell wall, but
this is not the case
  - It was eventually shown that the [[Chytridiomycota]] was a member of the
  [[Opisthokonta]]

## Taxonomy

- [[Fungi]]
  - [[Chytridiomycota]]
  - Amastigomycota
    - [[Glomeromycota]]
    - Dikarya
      - [[Ascomycota]]
      - [[Basidiomycota]]
