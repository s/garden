# Java

- A [[programming language]].
- Compiled to bytecode that is executed on the [[Java Virtual Machine]].
- Effectively platform-independent.
