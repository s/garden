# Opisthokonta

- Part of the [[Unikonta]]
- Named from "rear pole"
- Having a back-attaching flagellum is rare among [[Eukarya]]
