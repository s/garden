# Metamonada

- A [[phylum]] of the [[Excavata]] which lacks [[mitochondrion|mitochondria]]
  - This means they're adapted to anaerobic environments
- Includes  [[Diplomonadida]] and [[Parabasalia]]
