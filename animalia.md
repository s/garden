# Animalia

- Also known as [[Metazoa]]
- A member of the [[Opisthokonta]]
- Multicellular
- Most basal of Metazoa: [[Sponge]]
  - Water enters through the [[ostia]] and exits through the [[osculum]]
  - [[flagellated chamber]]
    - Separating the ostia and the [[spongocoel]]
  - Different cell types
    - Outer layer: [[pinacoderm]]
    - Controlling the ostia: [[porocytes]]
    - Controlling the flagella, eating: [[choanocytes]]
    - Transport nutrition: [[amoebocytes]]
- A few taxa of interest with respect to the origin of animals:
  - [[sponge|Sponges]]
  - [[cnidaria|Cnidarians]]
  - [[Ctenophores]]
  - [[Bilateria]]
- Goes through [[cleavage-divisions|Cleavage Divisions]]
- Required to be [[multicellular]] and go through [[morphogenesis]]

## Taxonomy

- [[Animalia]]
  - [[Porifera]] (Parazoa)
    - [[sponge|Sponges]]
  - [[Eumetazoa]] (2 germ layers, ecto- and endo-, "true tissues")
    - [[Radiata]]
      - [[Cnidaria]]
    - [[Bilateria]] (3 germ layers, meso-)
      - [[Deuterostomia]]
        - [[Echinodermata]]
        - [[Chordata]]
      - [[Protostomia]]
        - [[Lophotrochozoa]]
          - [[Annelida]]
          - [[Mollusca]]
          - Flatworms
        - [[Ecdysozoa]]
          - [[Arthropoda]]
          - [[Nematoda]]
