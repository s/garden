# Apicomplexa

- Members of the [[Alveolata]] [[pyhlum]] of the [[sar-supergroup|SAR Supergroup]]
- Plasmodium Falciparum
  - Causes malaria
- Toxoplasma Gondii
  - Normal cycle of infection revolves around mice and house cats
  - Makes mice act arrogant around predators to transmit to cats
  - Cat feces carries the parasite, which ends up being transmitted to mice
