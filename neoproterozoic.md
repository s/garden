# Neoproterozoic

- Last part of the [[Proterozoic]]
- During the [[Cryogenian]], there were two long glaciation periods where the
  Earth was mostly covered in ice
- Ends with the [[Ediacaran]]
