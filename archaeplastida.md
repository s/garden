# Archaeplastida

- Original [[eukarya|eukaryotic]] phototrophe, first to take in a [[cyanobacteria|cyanobacterium]] as
an organelle
- Two lineages:
  - [[Rhodoyhpta]]: Adapted to deeper water by having pigments more absorbant
  of blues
  - [[Viridaeplantae]]: Includes the bulk of the Archaeplastida, including the
  [[Chlorophyta]] and the [[Streptophyta]]
