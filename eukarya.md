# Eukarya

- There are two parts to the origin of eukaryotic cells:
  - [[Endosymbiosis]]: Theorized by [[Lynn Margulis]], proposes that
  mitochondria, along with other eukaryotic organelles, first existed as
  independent organisms
  - [[Endomembrane System]]: Documented by [[Christian de Duve]], the
  endomembrane system involves parts of organelles specific to eukaryotic
  organelles, such as the nuclear envelope
- In eukaryotes, [[mitochondrion|mitochondria]] have separate DNA from the nuclear genome,
which is evidence for [[Endosymbiosis]]

## Taxonomy

- [[Eukarya]]
  - Bikonta
    - [[Excavata]]
      - [[Euglenozoa]]
      - [[Metamonada]]
        - Trichozoa
          - [[Diplomonadida]]
          - [[Parabasalia]]
    - Diaphoretickes
      - [[sar-supergroup|SAR Supergroup]]
        - Rhizaria
          - Cercozoa
          - Retaria
            - Foraminifera
            - Radiolaria
        - Halvaria
          - [[Alveolata]]
            - [[Ciliophora]]
            - Myzozoa
              - [[Apicomplexa]]
              - [[Dinoflagellata]]
          - [[Heterokonta]] (Stramenopiles)
            - [[Diatoms]]
            - [[Oomycota]]
            - [[brown-algae|Brown algae]]
      - [[Archaeplastida]]
        - [[Rhodoyhpta]] (red algae)
        - [[Viridiplantae]]
          - [[Chlorophyta]]
          - [[Streptophyta]]
            - Charophyta
            - [[Embryophyta]] (land plants)
  - [[Unikonta]]
    - [[Amoebozoa]]
      - Tubulinea
      - Entamoeba
      - [[Eumycetozoa]] (slime molds)
    - [[Opisthokonta]]
      - Holomycota
        - Nucleariida
        - [[Fungi]]
      - Holozoa
        - [[Choanoflagellatea]]
        - [[Animalia]]
