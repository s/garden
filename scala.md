# Scala

- https://scala-lang.org/
- A [[programming-language|programming language]] that mainly runs on the [[java-virtual-machine|Java Virtual Machine]].
- Scala can be compiled to JavaScript or via LLVM as well.
