# The Legend of Zelda: Ocarina of Time

- A [[video-game|video game]] released on November 21, 1998 for the Nintendo 64.
- One of the highest rated casual games of all time.
- Part of [[the-legend-of-zelda|The Legend of Zelda]] series.
