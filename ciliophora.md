# Ciliophora

- Members of the [[Alveolata]] [[pyhlum]] of the [[sar-supergroup|SAR Supergroup]]
- Have hundreds to thousands of cilia used for feeding and mobility
- Abundant and diverse in most aquatic habitats
