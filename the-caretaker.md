# The Caretaker

- A character in a project by [[leyland-james-kirby|Leyland James Kirby]].
- A story is told from The Caretaker's perspective about living and dying with
  increasingly severe dementia that becomes Alzheimer's in late life.
