# Abiogenesis

- Life had to have some way to originate from non-living materials
  1. The abiotic (nonliving) synthesis of small organic molecules, such as
  amino acids and nitrogenous bases
  2. The joining of these small molecules into macromolecules, such as proteins
  and nucleic acids
  3. The packaging of these molecules into protocells, droplets with membranes
  that maintained an internal chemistry different from that of their
  surroundings
  4. The origin of self-replicating molecules ([[autocatalytic-set|autocatalytic set]]) that
  eventually made inheritance possible
- [[rna-world|RNA World]]: It's proposed that RNA was the first self-replicating molecule
