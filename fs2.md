# fs2

- https://fs2.io
- A [[Scala]] library that allows abstracting data transmission and control
  into a shared concept called a `Stream`.
